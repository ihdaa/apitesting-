package propFileConfiguration;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class InitalizeProparityFile {

	public static Properties inputProperties;
	public static Properties configProperties;
	protected InputStream inputstream;
	
public static void initializePropertyFiles() throws IOException {
		
		try {
		inputProperties=new Properties();
		InputStream InputStreamFile=InitalizeProparityFile.class.getClassLoader()
				.getResourceAsStream("propFileConfiguration/config.properties");
		
		
		if (InputStreamFile != null) {
			inputProperties.load(InputStreamFile);
		} else {
			throw new FileNotFoundException("Input path file not found ");

		}
		
		}catch (Exception e) {
			System.out.println("The exception happened at initializePropertyFiles function");	
			System.out.println("Exception: " + e);
		}		
}

}
