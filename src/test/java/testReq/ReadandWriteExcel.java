package testReq;


import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.testng.annotations.Test;
 
 
public class ReadandWriteExcel {
 
 public static void main(String []args){
  
  try {
  // Specify the path of file
  File src=new File("C:/Users/idweikat/Desktop/Test.xlsx");
 
   // load file
   FileInputStream fis=new FileInputStream(src);
 
   // Load workbook
   XSSFWorkbook wb=new XSSFWorkbook(fis);
   
   // Load sheet- Here we are loading first sheetonly
      XSSFSheet sheet1= wb.getSheetAt(0);
 
  // getRow() specify which row we want to read.
 
  // and getCell() specify which column to read.
  // getStringCellValue() specify that we are reading String data.
 
 
   /*String data00=sheet1.getRow(0).getCell(0).getStringCellValue();
   System.out.println(data00);
 
 System.out.println(sheet1.getRow(0).getCell(1).getStringCellValue());
 
 System.out.println(sheet1.getRow(1).getCell(0).getStringCellValue());
 
 System.out.println(sheet1.getRow(1).getCell(1).getStringCellValue());
 
 System.out.println(sheet1.getRow(2).getCell(0).getStringCellValue());
 
 System.out.println(sheet1.getRow(2).getCell(1).getStringCellValue());
 
 */
      //return number of row we have 
    int rowcount=  sheet1.getLastRowNum();
    System.out.println("***The total row is :"+rowcount);//start count from index 0
    
    for(int i=0;i<rowcount;i++) {
    	String data0=sheet1.getRow(i).getCell(0).getStringCellValue();
    	System.out.println("Data from row "+i+" is  "+data0);
    	String data1=sheet1.getRow(i).getCell(1).getStringCellValue();
    	System.out.println("Data from row "+i+" is  "+data1);
    }
 
 wb.close();
  } catch (Exception e) {
 
   System.out.println(e.getMessage());
 
  }
  
 }
 
}