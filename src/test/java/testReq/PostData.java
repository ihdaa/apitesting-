package testReq;

import org.json.simple.JSONObject;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import junit.framework.Assert;

public class PostData {
	
	@Test
	public void test1() {
		
		RequestSpecification request= RestAssured.given();
		request.header("Content-Type","application/json");
		JSONObject json= new JSONObject();
		json.put("id","2");
		json.put("titel","leader");
		json.put("author","ihdaaa dweikat");
				
		json.put("id","4");
		json.put("titel","leader");
		json.put("author","Bayan Omar");
		
		json.put("id","23");
		json.put("titel","leader");
		json.put("author","Hadi");
		
		request.body(json.toJSONString());
		Response response=request.post("http://localhost:3000/posts");////https://reqres.in/api/users
		int code=response.getStatusCode();
		System.out.println("Code is " +code);
		Assert.assertEquals(code, 201);
		
	}
	

}
