package setup;

import java.io.FileInputStream;

import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import excelConfiguration.ExcelDataConfig;
import io.restassured.response.Response;
import propFileConfiguration.GetInputProperties;
import propFileConfiguration.InitalizeProparityFile;

public class TheBase {

	public static int sheetNumber;
	public static int deleteId;
	public static int getId;
	public static int postId;
	public static XSSFWorkbook wb;
	public static XSSFSheet sheet1;
	public static XSSFSheet sheet2;
	public static XSSFSheet sheet3;
	public static FileInputStream fis;
	public static int code;
	public static Response resp;
	
	@BeforeTest
	public void beforetest() throws Exception {
		InitalizeProparityFile.initializePropertyFiles();
		ExcelDataConfig.ExcelConfig(GetInputProperties.path);	
		sheetNumber=Integer.parseInt(GetInputProperties.sheetNumber);
		deleteId=Integer.parseInt(GetInputProperties.deleteId);
		getId=Integer.parseInt(GetInputProperties.getId);
		postId=Integer.parseInt(GetInputProperties.postId);
	}
	
	@AfterTest
	public void Aftertest() throws Exception {
		ExcelDataConfig.DeleteColomunFromExcel(GetInputProperties.path);
		//System.out.println("Theeeeeee Enddddddd");
		
	}
}
