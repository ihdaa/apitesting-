package requestImplementationPages;
import org.json.simple.JSONObject;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import junit.framework.Assert;
import excelConfiguration.ExcelDataConfig;

public class PostImplementationPage extends ExcelDataConfig {

	public static String PostAllData(int sheetNumber) {
	
		int data0 = 0;
		String data1= null;
		String data2= null;
		int row=1;
		//int column=0;
		
		int rowcount=ExcelDataConfig.getRow();		
	    RequestSpecification request= RestAssured.given();
		request.header("Content-Type","application/json");
		JSONObject json= new JSONObject();
		
	    for(int i=0;i<rowcount;i++) {
	  
	        data0=(int) sheet1.getRow(row).getCell(0).getNumericCellValue();
	    	//String data0=sheet1.getRow(i).getCell(0).getStringCellValue();
	        //System.out.println("Data from row "+i+" is  "+data0);
	        data1=sheet1.getRow(row).getCell(1).getStringCellValue();
	    	//System.out.println("Data from row "+i+" is  "+data1);
	    	data2=sheet1.getRow(row).getCell(2).getStringCellValue();
	    	//System.out.println("Data from row "+i+" is  "+data2);
	    	
	    	json.put("id",data0);
			json.put("titel",data1);
			json.put("author",data2);
			
			request.body(json.toJSONString());
			Response response=request.post(ExcelDataConfig.getURL(1));////https://reqres.in/api/users
			int code=response.getStatusCode();
			//System.out.println("Code is " +code);
			Assert.assertEquals(code, 201);
			
	    	row=row+1;
	    }
	    
    	//column=column+1;
    	return (data0 +""+ data1+""+data2);	
	   
	}
}
