package requestImplementationPages;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import junit.framework.Assert;
import excelConfiguration.ExcelDataConfig;
import propFileConfiguration.GetInputProperties;
import setup.TheBase;


public class DeleteImplementationPage extends TheBase {

	public static int deleteById(int sheetNumber,int id) {
	   
		    int deleteId=Integer.parseInt(GetInputProperties.deleteId);
	        RequestSpecification request= RestAssured.given();
	    	Response response=request.delete(ExcelDataConfig.getURL(1)+"/"+deleteId);
			int code=response.getStatusCode();
			//System.out.println("Code is " +code);
			Assert.assertEquals(code, 200);
		
    	return (code);	
	}	
	
	public static int deleteAllData(int sheetNumber) {
		int data0 = 0;
		int row=1;
		
		try {

		
			
		int rowcount=ExcelDataConfig.getRow();
		
	    RequestSpecification request= RestAssured.given();
	
	    for(int i=0;i<rowcount;i++) {
	  
	        data0=(int) sheet1.getRow(row).getCell(0).getNumericCellValue();   
	        resp=request.delete(ExcelDataConfig.getURL(1)+"/"+data0);
	    	CheckReturnResult.StatusCode(row);
	    	row=row+1;
	    }
	    
    	//return (data0);	
	    
	}
		catch (Throwable e) {
			// TODO Auto-generated catch block
		System.out.println("This error happen inside DeleteImplementation");
		System.out.println(e.getMessage());
	    e.printStackTrace();
		}
		return data0;
	}
}
