package requestImplementationPages;

import excelConfiguration.ExcelDataConfig;
import propFileConfiguration.GetInputProperties;
import setup.TheBase;
import io.restassured.RestAssured;
import junit.framework.Assert;

public class GetImplementationPage extends TheBase{
	
	public static int getById(int sheetNumber,int id)  {
		
		int getId=Integer.parseInt(GetInputProperties.getId);
	    resp=RestAssured.get(ExcelDataConfig.getURL(1)+"?id="+getId);
		//System.out.println("resp is "+ resp.toString());
		//String data=resp.asString();
		//1System.out.println( data);
		//System.out.println("Responce time is "+ resp.getTime());
		
		code=resp.getStatusCode();
		//System.out.println("Status Code is "+ code);
		Assert.assertEquals(code,200);
		
        //Parsing value from respose jason
		String author = resp.jsonPath().getString("author");
		System.out.println("author is " + author);
		
		String iid = resp.jsonPath().getString("id");
		System.out.println("iid is " + iid);
		
		return code;		
	}
	
	
	public static int getAllData(int sheetNumber)  {
		int data0 = 0;
		int row=1;	////////////////////hon dynamic
		int rowcount=ExcelDataConfig.getRow();
		
		try {
			
	    for(int i=0;i<rowcount;i++) {
	  
	        data0=(int) sheet1.getRow(row).getCell(0).getNumericCellValue();
	    	String data1=sheet1.getRow(row).getCell(2).getStringCellValue();
	    	//System.out.println("Data from row "+i+" is  "+data0);
		    resp=RestAssured.get(ExcelDataConfig.getURL(1)+"?id="+data0);
			//String data=resp.asString();
			//System.out.println("data is "+ data);
			//System.out.println("Responce time is "+ resp.getTime());		  
			CheckReturnResult.StatusCode(row);////////////
			CheckReturnResult.ReturnData(data1);
			
	        row=row+1;
	    }


	}

			catch (Throwable e) {
					// TODO Auto-generated catch block
				System.out.println(e.getMessage());
			    e.printStackTrace();
				}
		
	 return (data0);
		
			}
	
}
