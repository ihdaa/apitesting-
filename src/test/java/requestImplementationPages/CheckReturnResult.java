package requestImplementationPages;

import excelConfiguration.ExcelDataConfig;
import junit.framework.Assert;
import propFileConfiguration.GetInputProperties;

public class CheckReturnResult extends GetImplementationPage {

	public static int code;
	
	public static void StatusCode(int row)  {
		
	    code=resp.getStatusCode();
		//System.out.println("Status Code is "+ code);
		int statusColumn=Integer.parseInt(GetInputProperties.statusColumn);	
		if(code==200) {
		
				ExcelDataConfig.WritePassOnExcel(GetInputProperties.path, row,statusColumn);
				Assert.assertEquals(code,200);
			} 

		else {
			ExcelDataConfig.WriteFailOnExcel(GetInputProperties.path, row, statusColumn);
			Assert.fail();
		}
		//return row;
	}	
	
	
	
	public static void ReturnData(String data1) {
		
		String author = resp.jsonPath().getString("author");
		//System.out.println("author is " + author.toString());
		String dataValue=data1.toString();
		//System.out.println("******data is ******" + dataValue);
		String authorValue=author.toString().replace("[", "").replace("]", "").trim();
		//System.out.println("------ author is ----  "+authorValue);
		//assertEquals(ggg,sss);	
		if(dataValue.equals(authorValue)) {
	     Assert.assertEquals(dataValue,authorValue);
		//assertTrue(true, "Same data");
		System.out.println("have the Same data ");
		Assert.assertTrue(true);
	}
	else {
		System.out.println("The data is different or threre is missing data");
		Assert.fail();
	}
	
	}

}
