package update;

import org.json.simple.JSONObject;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import junit.framework.Assert;

public class UpdateData {
	
	
	@Test
	public void test1() {
		
		RequestSpecification request= RestAssured.given();
		request.header("Content-Type","application/json");
		JSONObject json= new JSONObject();
		
		
		
		json.put("id","23");
		json.put("titel","leader");
		json.put("author","Amel");
		
		request.body(json.toJSONString());
		Response response=request.put("http://localhost:3000/posts/23");////https://reqres.in/api/users
		int code=response.getStatusCode();
		System.out.println("Code is " +code);
		Assert.assertEquals(code, 200);
		
	}
	



}
