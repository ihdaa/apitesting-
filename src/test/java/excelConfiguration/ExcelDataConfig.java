package excelConfiguration;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import propFileConfiguration.GetInputProperties;
import setup.TheBase;
import java.util.List;
import org.apache.poi.ss.usermodel.Cell;


public class ExcelDataConfig extends TheBase {


	
	public static void ExcelConfig (String excelPath) {
		   
		   try {
			File src=new File(excelPath);
			// load file
			fis=new FileInputStream(src);
			// Load workbook
			wb=new XSSFWorkbook(fis);
			// Load sheet- Here we are loading first sheetonly
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			System.out.println(e.getMessage());
			e.printStackTrace();
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			System.out.println(e.getMessage());
			e.printStackTrace();
		}
		
		
	}
	
	
	public static void WritePassOnExcel (String excelPath,int row,int column) {
		   
		   try {
			   
			ExcelDataConfig.ExcelConfig(excelPath);
			sheet1= wb.getSheetAt(0);			
			sheet1.getRow(row).createCell(column).setCellValue("Pass");
			FileOutputStream fout=new FileOutputStream(new File(GetInputProperties.path));			 
			// finally write content 			 
			 wb.write(fout);			 
			// close the file		 
			 fout.close();
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			System.out.println(e.getMessage());
			e.printStackTrace();
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			System.out.println(e.getMessage());
			e.printStackTrace();
		}	
		
	}
	
	public static void WriteFailOnExcel (String excelPath,int row,int column) {
		   
		   try {
			   
			ExcelDataConfig.ExcelConfig(excelPath);
			sheet1= wb.getSheetAt(0);	
			sheet1.getRow(row).createCell(column).setCellValue("Fail"); 		
			FileOutputStream fout=new FileOutputStream(new File(GetInputProperties.path));
			// finally write content 	 
			 wb.write(fout);		 
			// close the file		 
			 fout.close();
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			System.out.println(e.getMessage());
			e.printStackTrace();
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			System.out.println(e.getMessage());
			e.printStackTrace();
		}	
		
	}
	
	
	public static void DeleteColomunFromExcel (String excelPath) {
		   
	
	try {
		
	   ExcelDataConfig.ExcelConfig(excelPath);
	   sheet1= wb.getSheetAt(0);
	   int statusColumn=Integer.parseInt(GetInputProperties.statusColumn);	
       int rowcount=  sheet1.getLastRowNum();
       rowcount=rowcount+1;
       for(int i=1;i<rowcount;i++) {
    	   Row row = wb.getSheetAt(0).getRow(i);
    	   row.removeCell(row.getCell(statusColumn));   
    	   System.out.println("delete done");
       }
                         
        fis.close(); //Close the InputStream
        FileOutputStream output_file =new FileOutputStream(new File(GetInputProperties.path));  //Open FileOutputStream to write updates       
        wb.write(output_file); //write changes
        output_file.close();  //close the stream  
	}
	
	catch (Exception e) {
		// TODO Auto-generated catch block
		System.out.println(e.getMessage());
		e.printStackTrace();
	}
		
	}

	
	
	public static String getURL(int sheetNumber) {
		sheet2= wb.getSheetAt(sheetNumber);
		String url=sheet2.getRow(0).getCell(1).getStringCellValue();
		//System.out.println("The url is "+url);
		return url;
	}
	

	public static int getRow() {
		int sheetNumber = 0;
		sheet1= wb.getSheetAt(sheetNumber);
		int rowcount=  sheet1.getLastRowNum();
		//rowcount=rowcount+1;
		System.out.println("***The total row is :"+rowcount);//start count from index 0 
		return rowcount;		
	}
	
	
	
	
}
